package ru.fnight.libtaskbackpackfast

object BackpackFast {
  /** Main method to solve task about backpack
    *
    * @param M  total size capacity of backpack
    * @param m1 array with weights of items (N ints)
    * @param c1 array with prices of items (N ints)
    * @return array with selected items
    */
  def solve(M: Int, m1: Array[Int], c1: Array[Int]): Array[Int] = {
    findByWeight(
      mapItems(0, m1.toList, c1.toList)
        .sortWith((item1, item2) => item1.p / item1.w > item2.p / item2.w), 0, M)
      .map(_.i).toArray
  }

  def copyArray(array: Array[Int]): List[Int] = array.length match {
    case 0 => List()
    case _ => array(0) :: copyArray(array.drop(0))
  }

  def findByWeight(items: List[Item], currentW: Int, maxW: Int): List[Item] = items match {
    case h :: t if maxW >= currentW + h.w => h :: findByWeight(t, currentW + h.w, maxW)
    case _ => List()
  }

  def mapItems(index: Int, m1: List[Int], c1: List[Int]): List[Item] = m1 match {
    case head :: tail => new Item(index, head, c1.head) :: mapItems(index + 1, tail, c1.tail)
    case _ => List()
  }
}
